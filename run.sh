#!/usr/bin/env bash

sh wait-for.sh -t 10 db:5432
python3 manage.py migrate --noinput
python3 manage.py loaddata \
  ./account/fixtures/users.json \
  ./qa/fixtures/tags.json \
  ./qa/fixtures/questions.json \
  ./qa/fixtures/answers.json
uwsgi --uwsgi 0.0.0.0:8000 --wsgi hasker.wsgi:application --static-map /media/=/opt/hasker/media
