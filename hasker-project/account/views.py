from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.views import LoginView
from django.urls import reverse_lazy
from django.views.generic import CreateView, UpdateView

from account.models import User
from hasker.mixins import UnauthenticatedOnlyMixin
from .forms import SignupForm, AccountSettingsForm


class UpdateAccount(LoginRequiredMixin, UpdateView):
    model = User
    form_class = AccountSettingsForm
    template_name = 'account/account.html'
    success_url = reverse_lazy('account')

    def get_object(self):
        return self.request.user


class SignUp(UnauthenticatedOnlyMixin, CreateView):
    form_class = SignupForm
    success_url = reverse_lazy('login')
    template_name = 'account/signup.html'


class Login(LoginView):
    template_name = 'account/login.html'
    redirect_authenticated_user = False
