from django.contrib.auth.models import (
    AbstractUser,
    UserManager as BaseUserManager
)
from django.db import models

from account.managers import UserManager


class User(AbstractUser):
    avatar = models.ImageField(upload_to='avatars/', blank=True)
    objects = BaseUserManager()
    users = UserManager()

    def __str__(self):
        return self.username
