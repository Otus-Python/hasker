from django.urls import path
from django.contrib.auth.views import LogoutView

from . import views

urlpatterns = [
    path('signup/', views.SignUp.as_view(), name='signup'),
    path('login/', views.Login.as_view(), name='login'),
    path('logout/', LogoutView.as_view(next_page='index'), name='logout'),
    path('', views.UpdateAccount.as_view(), name='account'),
]
