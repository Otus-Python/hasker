from django import forms
from django.contrib.auth.forms import UserCreationForm

from .models import User


class SignupForm(UserCreationForm):
    email = forms.EmailField(
        max_length=254,
        label='Email',
        help_text='Field is required',
    )

    class Meta:
        model = User
        fields = ('username', 'email', 'password1', 'password2', 'avatar' )
        labels = {
            'username': 'Login',
            'email': 'Email',
            'password1': 'Password',
            'avatar': 'Avatar'
        }

    def __init__(self, *args, **kwargs):
        super(SignupForm, self).__init__(*args, **kwargs)
        self.fields['password2'].label = 'Confirm Password'


class AccountSettingsForm(forms.ModelForm):

    class Meta:
        model = User
        fields = ('username', 'email', 'avatar')
        labels = {
            'username': 'Login',
            'email': 'Email',
            'avatar': 'Avatar',
        }

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['username'].disabled = True
