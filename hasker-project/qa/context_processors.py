from hasker.utils import get_settings_value
from .models import Question


def trending_questions(_):
    limit = get_settings_value('TRENDING', 20)
    return {
        'trending_questions': Question.objects.all().order_by('-rating')[:limit]
    }
