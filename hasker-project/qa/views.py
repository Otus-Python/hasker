from django.contrib.auth.decorators import login_required
from django.db import transaction
from django.shortcuts import render, redirect, get_object_or_404
from django.urls import reverse_lazy
from django.views.decorators.http import require_POST

from hasker.utils import wrap_with_paginator, get_settings_value
from qa.forms import QuestionForm, AnswerForm
from .models import Question, Answer


def index(request):
    order_by = request.GET.get('sort', 'id')
    page = request.GET.get('page', 1)
    questions_list = Question.questions.order_by("-{}".format(order_by))
    questions = wrap_with_paginator(
        questions_list, page, get_settings_value('PAGINATE_QUESTIONS', 20)
    )
    return render(request, 'qa/main.html', {
        'questions': questions
    })


@login_required(login_url=reverse_lazy('login'))
def create_question(request):
    if request.method == 'POST':
        form = QuestionForm(request.POST)
        if form.is_valid():
            with transaction.atomic():
                q = form.save(commit=False)  # type: Question
                q.save_author_and_tags(request)
        return redirect('index')  # TODO change destination

    form = QuestionForm()
    return render(request, 'qa/create_question.html', {
        'form': form,
    })


def show_question(request, slug):
    q = get_object_or_404(Question.questions, slug=slug)
    page = request.GET.get('page', 1)
    answers_list = Answer.answers.filter(question=q)
    answers_list = answers_list.order_by('-rating', 'created_at')
    answers = wrap_with_paginator(
        answers_list, page, get_settings_value('PAGINATE_ANSWERS', 30)
    )
    return render(request, 'qa/show_question.html', {
        'question': q,
        'form': AnswerForm(),
        'answers': answers
    })


@login_required(login_url=reverse_lazy('login'))
@require_POST
def create_answer(request, slug):
    form = AnswerForm(request.POST)
    if form.is_valid():
        a = form.save(commit=False)  # type: Answer
        a.bind_with_question_and_user(slug, request.user)
        a.send_notification(request)
    return redirect('show_question', slug)


@login_required(login_url=reverse_lazy('login'))
@require_POST
def vote_answer(request, slug, pk, value):
    a = get_object_or_404(Answer, pk=pk)
    a.update_rating(request.user, int(value))
    return redirect('show_question', slug)


@login_required(login_url=reverse_lazy('login'))
@require_POST
def vote_question(request, slug, value):
    q = get_object_or_404(Question, slug=slug)
    q.update_rating(request.user, int(value))
    return redirect('show_question', slug)


@login_required(login_url=reverse_lazy('login'))
@require_POST
def approve_answer(request, slug, pk):
    a = get_object_or_404(Answer, pk=pk)
    a.approve()
    return redirect('show_question', slug)
