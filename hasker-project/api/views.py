from rest_framework import viewsets, permissions
from rest_framework.decorators import action
from rest_framework.response import Response

from account.models import User
from qa.models import Question, Answer
from .permissions import IsOwnerOrReadOnly
from .serializers import (
    UserSerializer,
    QuestionSerializer,
    AnswerSerializer
)


class UserViewSet(viewsets.ReadOnlyModelViewSet):
    """
    Show users
    """
    queryset = User.users.all()
    serializer_class = UserSerializer

    permission_classes = (
        permissions.IsAuthenticatedOrReadOnly,
        permissions.IsAdminUser
    )


class QuestionViewSet(viewsets.ModelViewSet):
    """
    Show, create and edit questions
    """
    queryset = Question.api_questions.all()
    serializer_class = QuestionSerializer

    permission_classes = (
        permissions.IsAuthenticatedOrReadOnly,
        IsOwnerOrReadOnly,
    )

    def perform_create(self, serializer):
        serializer.save(author=self.request.user)

    @action(detail=False, )
    def trending(self, request, *args, **kwargs):
        queryset = self.get_queryset()
        queryset = queryset.order_by('-rating')
        return self.prepare_paginated_response(queryset)

    @action(detail=False, methods=permissions.SAFE_METHODS)
    def search(self, request, *args, **kwargs):

        q = request.GET.get('q')
        if q is None:
            return Response({
                "error": "request must contain query parameter q"
            }, status=400)
        queryset = Question.search(q)
        queryset.order_by("-rating")
        return self.prepare_paginated_response(queryset)

    def prepare_paginated_response(self, queryset):
        page = self.paginate_queryset(queryset)
        if page is not None:
            serializer = self.get_serializer(page, many=True)
            return self.get_paginated_response(serializer.data)

        serializer = self.get_serializer(queryset)
        return Response(serializer.data)


class AnswerViewSet(viewsets.ModelViewSet):
    """
    Show, create and edit answers
    """
    queryset = Answer.answers.all()
    serializer_class = AnswerSerializer
    permission_classes = (
        permissions.IsAuthenticatedOrReadOnly,
        IsOwnerOrReadOnly,
    )

    def perform_create(self, serializer):
        serializer.save(author=self.request.user)
