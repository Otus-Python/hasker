from rest_framework import serializers

from account.models import User
from qa.models import Question, Answer


class UserSerializer(serializers.HyperlinkedModelSerializer):
    questions = serializers.HyperlinkedRelatedField(
        many=True, view_name='question-detail', read_only=True
    )

    class Meta:
        model = User
        fields = ('url', 'username', 'email', 'questions')


class QuestionSerializer(serializers.HyperlinkedModelSerializer):
    author = serializers.ReadOnlyField(source='author.username')
    answers = serializers.HyperlinkedRelatedField(
        many=True, view_name='answer-detail', read_only=True
    )

    class Meta:
        model = Question
        read_only_fields = ('rating', )
        fields = ('url', 'author', 'rating', 'title', 'content', 'answers')


class AnswerSerializer(serializers.HyperlinkedModelSerializer):
    author = serializers.ReadOnlyField(source='author.username')
    question = serializers.HyperlinkedRelatedField(
        view_name='question-detail', queryset=Question.objects.all()
    )

    class Meta:
        model = Answer
        read_only_fields = ('rating', 'author', )
        fields = ('url', 'author', 'rating', 'content', 'question', )
