from django.conf import settings
from django.conf.urls.static import static
from django.contrib import admin
from django.urls import path, include
from drf_yasg import openapi
from drf_yasg.views import get_schema_view
from rest_framework import permissions

import qa.views

schema_view = get_schema_view(
   openapi.Info(
      title="Hasker API",
      default_version='v1',
      description="Hasker: The poor man's stackoverflow",
      terms_of_service="https://www.google.com/policies/terms/",
      contact=openapi.Contact(email="feedback@hasker.com"),
      license=openapi.License(name="Free License"),
   ),
   public=True,
   permission_classes=(
       permissions.AllowAny,
   ),
)


urlpatterns = [
    path('', qa.views.index, name='index'),
    path('account/', include('account.urls')),
    path('qa/', include('qa.urls')),
    path('search/', include('search.urls')),
    path('admin/', admin.site.urls),
    path('api/', include('api.urls')),
    path('schema/', schema_view.with_ui('swagger', cache_timeout=0), name='schema-swagger-ui'),
]

if settings.DEBUG:
    import debug_toolbar
    urlpatterns = [
        path('__debug__/', include(debug_toolbar.urls)),
    ] + urlpatterns + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
