from django.urls import path
from . import views

urlpatterns = [
    path('', views.search, name='search'),
    path('tag/<str:tag>', views.search_tag, name='search_tag')
]
