from django.shortcuts import render, redirect

from hasker.utils import wrap_with_paginator, get_settings_value
from qa.models import Question


def search(request):
    q = request.GET.get('q', '')  # type: str
    if q.startswith('tag:'):
        _, tag = q.split(':', 1)
        return redirect('search_tag', tag.strip())

    questions_list = Question.search(q)
    questions_list = questions_list.order_by('-rating', 'created_at')
    page = request.GET.get('page', 1)
    questions = wrap_with_paginator(
        questions_list, page, get_settings_value('PAGINATE_SEARCH_RESULTS', 20)
    )
    return render(request, 'search/results.html', {
        'questions': questions,
        'q': q,
    })


def search_tag(request, tag):
    question_list = Question.questions.filter(tags__title__icontains=tag)
    # most popular and most recent
    question_list = question_list.order_by('-rating', '-created_at')
    page = request.GET.get('page')
    questions = wrap_with_paginator(
        question_list, page, get_settings_value('PAGINATE_SEARCH_RESULTS', 20)
    )
    return render(request, 'search/tag_results.html', {
        'questions': questions,
        'q': 'tag:{}'.format(tag)
    })
