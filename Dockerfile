FROM python:alpine

# Installing client libraries and any other package you need
RUN apk update && apk add libpq jpeg make
# Installing build dependencies
RUN apk add --virtual .build-deps gcc python3-dev musl-dev \
    postgresql-dev linux-headers zlib-dev jpeg-dev
# Installing and build python module
RUN pip install psycopg2 uwsgi Pillow
# Delete build dependencies
RUN apk del .build-deps

RUN mkdir -p /opt/hasker
WORKDIR /opt/hasker
ADD requirements.txt /opt/hasker
ADD constraints.txt /opt/hasker
RUN pip install -r requirements.txt -c constraints.txt
ADD hasker-project /opt/hasker
ADD run.sh /opt/hasker
ADD wait-for.sh /opt/hasker

CMD ["sh","run.sh"]
